" language en_US

call plug#begin('~/.vim/plugged')

" Use release branch (recommend)
Plug 'neoclide/coc.nvim', {'branch': 'release'}

Plug 'vim-airline/vim-airline'

Plug 'preservim/nerdtree'

" put commentary with the gc operator
Plug 'tpope/vim-commentary'
" cs : provides mappings to del, chg, add surroundings in pairs.
Plug 'tpope/vim-surround'
" Complementary pairs of mappings. Mostly fall into four categories.
Plug 'tpope/vim-unimpaired'
" Repeat.vim remaps . in a way that plugins can tap into it.
Plug 'tpope/vim-repeat'


"" TO TEST Plugins
"Plug 'jiangmiao/auto-pairs'
"Plug 'junegunn/fzf.vim'

" Initialize plugin system
call plug#end()


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"""""""""""""""""""" SPECIFICATIONS """""""""""""""""""""""""""""
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

""" General specs """

" set language en
" set language='en_US.UTF-8'

"" Lines number info
set number
set relativenumber

"" Tab
set tabstop=4
set softtabstop=4
" autoindent number of spaces
set shiftwidth=4
set expandtab

" Move
set mouse=a "enable the use of the mouse ('a'=all)

" Search
nnoremap <CR> :nohlsearch<CR><CR> " rm search highlight
set inccommand=split " search through multiple windows

" autocomplete files path relative to current file (not terminal path)
set autochdir

set list " show invisibles
set listchars=tab:▸\ ,eol:¬ " show invisibles options
highlight NonText guifg=#4a4a59
highlight SpecialKey guifg=#4a4a59

""" Plugin specs """

"" Airline specs
" Pythonenv info
let g:airline#extensions#poetv#enabled = 1

"" HardTime specs
" let g:hardtime_default_on = 1
let g:hardtime_ignore_buffer_patterns = [ "NERD.*" ]
" let g:hardtime_timeout = 10
" let g:hardtime_maxcount = 3

"" Nerdtree spec

"" Open Nerdtree at startup
autocmd vimenter * NERDTree
"" Move cursor to file editing area at startup
autocmd VimEnter * wincmd p
"" Close Nerdtree if last window
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

"" Coc spec

" correct comment highlighting for coc config files (jsonc)
 autocmd FileType json syntax match Comment +\/\/.\+$+

 " Use tab for trigger completion with characters ahead and navigate.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

" Make <CR> auto-select the first completion item and notify coc.nvim to
" format on enter, <cr> could be remapped by other vim plugin
inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

" Use `(g` and `)g` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
nmap <silent> (g <Plug>(coc-diagnostic-prev)
nmap <silent> )g <Plug>(coc-diagnostic-next)


 " GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  elseif (coc#rpc#ready())
    call CocActionAsync('doHover')
  else
    execute '!' . &keywordprg . " " . expand('<cword>')
  endif
endfunction

